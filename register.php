<?php
	
	
	require_once "init.php";
	
	$template = new DOMTemplate(file_get_contents ('templates/login_register.html'));
	
	$template->setValue('/html/head/title', 'Register');
	$template->setValue('#title', 'Register');
	$template->remove("#login");
	$template->remove("#reset_password");
	
	if($_SESSION['msg']){
		$template->setValue('#msg', $_SESSION['msg']);
		$_SESSION['msg'] = NULL;
	}
	
	echo $template;
	
	