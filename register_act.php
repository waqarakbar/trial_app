<?php
	
	require_once "init.php";
	
	$user_name = $_POST['user_name'];
	$password = $_POST['user_password'];
	$confirm_password = $_POST['confirm_user_password'];
	
	// check if user_password and confirm matches
	if($password != $confirm_password){
		$_SESSION['msg'] = "Sorry! Password and confirm password doesn't match";
		header("location:./register.php");
		exit;
	}
	
	
	
	try{
		
		$data = [
			'user_name' => $user_name,
			'user_password' => $password,
			'role_id' => '2'
		];
		
		Plusql::into($profile)->user($data)->insert();
		
		
		$_SESSION['msg'] = 'Account has been created successfully, please login with your credentials';
		header("location:./login.php");
		
	}catch (PluSQL\SqlErrorException $e){
		
		$_SESSION['msg'] = "Sorry! ".$e->getMessage();
		header("location:./register.php");
		
	}
	
	