<?php
	
	require_once "init.php";
	
	$message_to = $_POST['message_to'];
	$message_type_id = $_POST['message_type_id'];
	
	
	
	try{
		
		$data = [
			'message_to' => $_POST['message_to'],
			'message_type_id' => $_POST['message_type_id'],
			'message_text' => $_POST['message_text'],
			'user_id' => $_SESSION['user_id']
		];
		
		Plusql::into($profile)->message($data)->insert();
		
		
		$_SESSION['msg'] = 'New message has been sent successfully';
		header("location:./sent_messages.php");
		
	}catch (PluSQL\SqlErrorException $e){
		
		$_SESSION['msg'] = "Sorry! ".$e->getMessage();
		header("location:./sent_messages.php");
		
	}
	
	