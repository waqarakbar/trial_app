<?php
	
	require_once "init.php";
	
	//unset the sesssion array
	$_SESSION = array();
	
	$_SESSION['logged_in'] = FALSE;
	
	//if session stores cockie, delete it
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]
		);
	}
	
	//now destroy the session
	session_destroy();
	
	header("location:./login.php");
	exit;