<?php
	
	
	require_once "init.php";
	
	$template = new DOMTemplate(file_get_contents ('templates/new_message.html'));
	
	$template->setValue('/html/head/title', 'Send New Message');
	$template->setValue('#title', 'Send New Message');
	
	if($_SESSION['msg']){
		$template->setValue('#msg', $_SESSION['msg']);
		$_SESSION['msg'] = NULL;
	}
	
	
	// switch the menues based on role id of user
	if($_SESSION['role_id'] == 1){
		$template->remove('#message_sender_menu');
	}else{
		$template->remove('#super_user_menu');
	}
	
	
	echo $template;
	
	