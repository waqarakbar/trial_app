<?php
	
	
	require_once "init.php";
	
	$template = new DOMTemplate(file_get_contents ('templates/users.html'));
	
	$template->setValue('/html/head/title', 'All users');
	$template->setValue('#title', 'All users');
	
	// display any message from session
	if($_SESSION['msg']){
		$template->setValue('#msg', $_SESSION['msg']);
		$_SESSION['msg'] = NULL;
	}
	
	
	// switch the menues based on role id of user
	if($_SESSION['role_id'] == 1){
		$template->remove('#message_sender_menu');
	}else{
		$template->remove('#super_user_menu');
	}
	
	try{
		
		$user = Plusql::from($profile)
			->user
			->select('*')
			->where('user.role_id = 2')
			->run()->user;
		
		$item = $template->repeat('.item');
		
		foreach ($user as $u){
			$item->setValue('.sn', ++$sn);
			$item->setValue('.username', $u->user_name);
			$item->setValue('.email', $u->user_email);
			
			// now get the unique recipients
			$recipients = [];
			try{
				
				$recipient = Plusql::from($profile)
					->message
					->select('*')
					->where('message.user_id = '.$u->user_id)
					->run()->message;
				foreach($recipient as $r){
					$recipients[] = $r->message_to;
				}
				
				$unique_recipients = array_unique($recipients);
				
				$item->setValue('.recipient', implode(", ", $unique_recipients));
				
			}catch (EmptySetException $e){
				
			}
			
			
			$item->next();
		}
		
	}catch (EmptySetException $e){
		
		$e->getMessage();
		
	}
	
	
	
	
	
	
	echo $template;
	
	