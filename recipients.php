<?php
	
	
	require_once "init.php";
	
	$template = new DOMTemplate(file_get_contents ('templates/recipients.html'));
	
	$template->setValue('/html/head/title', 'All Recipients');
	$template->setValue('#title', 'All Recipients');
	
	// display any message from session
	if($_SESSION['msg']){
		$template->setValue('#msg', $_SESSION['msg']);
		$_SESSION['msg'] = NULL;
	}
	
	
	// switch the menues based on role id of user
	if($_SESSION['role_id'] == 1){
		$template->remove('#message_sender_menu');
	}else{
		$template->remove('#super_user_menu');
	}
	
	try{
		
		$message = Plusql::from($profile)
			->message
			->select('*')
			->run()->message;
		$recipients = [];
		foreach($message as $msg){
			$recipients[] = $msg->message_to;
		}
		$unique_recipients = array_unique($recipients);
		
		$item = $template->repeat('.item');
		
		foreach ($unique_recipients as $r){
			$item->setValue('.sn', ++$sn);
			$item->setValue('.to', $r);
			
			
			try{
				$user = Plusql::from($profile)
					->user
					->message
					->select('*')
					->where('message.message_to = "'.$r.'"')
					->run()->user;
				
				// unique senders
				$senders = [];
				foreach($user as $u){
					$senders[] = $u->user_name;
				}
				$unique_senders = array_unique($senders);
				
				$item->setValue('.senders', implode(', ', $unique_senders));
				
			}catch(EmptySetException $e){
				
			}
			
			
			$item->next();
		}
		
	}catch (EmptySetException $e){
		
		$e->getMessage();
		
	}
	
	
	
	
	
	
	echo $template;
	
	