<?php
	
	
	
	$exception = [
		'login.php',
		'login_act.php',
		'register.php',
		'register_act.php',
		'logout.php',
		'reset_password.php',
		'reset_password_act.php'
	];
	
	if(!$_SESSION['logged_in'] and !in_array($current_file, $exception)){
		header("location:./login.php");
		exit;
	}