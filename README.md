# README - UpWok Trial App #
This is trial project for sending messages

## Setup ##
* Clone the repository to your local webserver root
* Import the database dump (DB/trial_app.sql) to your local MySQL server 
* Change database user/password in libs/db.php
* Type http://localhost/trial_app in your browser to access the system
* Live demo can be accessed via http://waqarakbar.com/trial_app
* For usernames / passwords see Users levels
* Download zip from http://waqarakbar.com/trial_app.zip

## Users levels ##
* There are two user levels
* Master User (can view all users and recipients) 
* Username: master, password: 123456
* Normal User (can send messages to recipients and view there own list of messages)
* Username / password = sender1 / 123456
* Username / password = sender2 / 123456
* New normal users can register in the system by clicking register link on login page
* Existing users can retrieve their password or login to the system by entering their credentials

## Issues and Limitations ##
* There are still some issue/limitation in this system, described below
* As I could not find a good user manual for the RocketSled micro framework, so i built this system without it. However I still implemented "Template Animation" concept. All html templates are in /templates directory
* I was not able to find a manual for selecting unique entries for DB using PluSQL to I did it programatically in Users and Recipients modules
* User passwords are not hashed as I had to show it to them on password recovery page
* I could have implemented CSRFGuard as it relies on javascript and it was mentioned in requirement I am not supposed to use javascript, so the system is missing CSRF and XSS protection
* I haven't used PluSQL before therefore I have no idea if it is SQL injection safe or not, however it is mentioned in docs that it is safe
