<?php
	
	
	require_once "init.php";
	
	$template = new DOMTemplate(file_get_contents ('templates/login_register.html'));
	
	$template->setValue('/html/head/title', 'Login to Dashboard');
	$template->setValue('#title', 'Login to Dashboard');
	$template->remove("#register");
	$template->remove("#reset_password");
	
	if($_SESSION['msg']){
		$template->setValue('#msg', $_SESSION['msg']);
		$_SESSION['msg'] = NULL;
	}
	
	echo $template;
	
	