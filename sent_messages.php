<?php
	
	
	require_once "init.php";
	
	$template = new DOMTemplate(file_get_contents ('templates/sent_messages.html'));
	
	$template->setValue('/html/head/title', 'All sent messages');
	$template->setValue('#title', 'All sent messages');
	
	// display any message from session
	if($_SESSION['msg']){
		$template->setValue('#msg', $_SESSION['msg']);
		$_SESSION['msg'] = NULL;
	}
	
	
	// switch the menues based on role id of user
	if($_SESSION['role_id'] == 1){
		$template->remove('#message_sender_menu');
	}else{
		$template->remove('#super_user_menu');
	}
	
	try{
		
		$message = Plusql::from($profile)
			->message
			->message_type
			->select('*')
			->where('message.user_id = "'.$_SESSION['user_id'].'"')
			->orderBy('message.message_created_date desc')
			->run()->message;
		
		
		
		$item = $template->repeat('.item');
		
		foreach ($message as $m){
			$item->setValue('.sn', ++$sn);
			$item->setValue('.to', $m->message_to);
			$item->setValue('.type', $m->message_type_name);
			$item->setValue('.date', date("F d, Y h:ia", strtotime($m->message_created_date)));
			$item->next();
		}
		
	}catch (EmptySetException $e){
		
		$e->getMessage();
		
	}
	
	
	
	
	
	
	echo $template;
	
	