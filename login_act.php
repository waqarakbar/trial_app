<?php
	
	require_once "init.php";
	
	$user_name = $_POST['user_name'];
	$password = $_POST['user_password'];
	
	$f = Plusql::escape($profile);
	
	try{
		
		$user = Plusql::from($profile)->user
			->role
			->select('*')
			->where('user.user_name = "'.$f($user_name).'" and user.user_password = "'.$f($password).'"')
			->run()->user;
		
		foreach ($user as $u){
			$_SESSION['role_id'] = $u->role_id;
			$_SESSION['user_id'] = $u->user_id;
			$_SESSION['user_name'] = $u->user_name;
			$_SESSION['logged_in'] = TRUE;
		}
		
		$_SESSION['msg'] = 'Welcome to dashboard '.$_SESSION['user_name'];
		
		if($_SESSION['role_id'] == 1){
			header("location:./users.php");
		}else{
			header("location:./sent_messages.php");
		}
		
		
	}catch (EmptySetException $e){
		
		$_SESSION['msg'] = "Sorry! incorrect username or password";
		header("location:./login.php");
		
	}
	
	