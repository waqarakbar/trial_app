<?php
	
	
	require_once "init.php";
	
	$template = new DOMTemplate(file_get_contents ('templates/login_register.html'));
	
	$template->setValue('/html/head/title', 'Reset Password');
	$template->setValue('#title', 'Reset Password');
	$template->remove("#register");
	$template->remove("#login");
	
	if($_SESSION['msg']){
		$template->setValue('#msg', $_SESSION['msg']);
		$_SESSION['msg'] = NULL;
	}
	
	echo $template;
	
	